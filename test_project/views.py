from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def mojtaba_feature(request):
    return HttpResponse("mojtaba feature")


def zeroman_feature(request):
    return None


def zeroman_another_feature(request):
    return HttpResponse("another feature")


def conflict_table_for_test(request):
    return None


def kooft(request):
    return None


def temp(request):
    pass


def seomthing(request):
    return 123
